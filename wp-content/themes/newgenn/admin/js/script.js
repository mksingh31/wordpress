( function( $ ) {
	// Add Make Plus message
		upgrade = $('<a class="newgenn-buy-pro"></a>')
			.attr('href', 'http://www.webulousthemes.com/theme/newgenn-pro/')
			.attr('target', '_blank')
			.text(newgenn_upgrade.message)
		;
		demo = $('<a class="newgenn-docs"></a>')
			.attr('href','http://newgenn.webulous.in/')
			.attr('target','_blank')
			.text(newgenn_upgrade.demo);
		docs = $('<a class="newgenn-docs"></a>')
			.attr('href','http://docs.webulous.in/newgenn-free/')
			.attr('target','_blank')
			.text(newgenn_upgrade.docs);
		support = $('<a class="newgenn-docs"></a>')
			.attr('href','http://www.webulousthemes.com/forums/forum/free-support/newgenn/')
			.attr('target','_blank')
			.text(newgenn_upgrade.support);

		$('.preview-notice').append(upgrade);
		$('.preview-notice').append(demo); 
		$('.preview-notice').append(docs);
		$('.preview-notice').append(support);
		// Remove accordion click event
		$('.newgenn-buy-pro').on('click', function(e) {
			e.stopPropagation();
		});
		$('.newgenn-docs').on('click',function(e){
			e.stopPropagation();
		})
} )( jQuery );
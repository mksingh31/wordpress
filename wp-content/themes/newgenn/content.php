<?php
/**
 * @package NewGenn
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



<div class="entry-content">
	<?php 
		$newgenn = get_theme_mods(); 
		if( ! isset($newgenn['featured_image']) && has_post_thumbnail() ) : ?>
			<div class="post-thumb">
				<?php the_post_thumbnail(); ?>   
			</div>

		<?php else : 
				$featured_image = get_theme_mod( 'featured_image' );
				$featured_image_size = get_theme_mod ('featured_image_size','1');
					if( $featured_image && has_post_thumbnail() ) : ?>
						<div class="post-thumb">
							<?php if ( $featured_image_size == '1' ) : 
								 the_post_thumbnail('newgenn-blog-full-width');
								else: 
								 the_post_thumbnail('newgenn-small-featured-image-width');
								endif;
							?>    
						</div>			
					<?php 
					endif; 
		endif; ?>   


	<div class="entry-body">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s <span class="meta-nav">&rsaquo;</span>', 'newgenn' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		?>
	</div>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'newgenn' ),
				'after'  => '</div>',
			) );
		?>
	<br class="clear" />
</div>
	
</article><!-- #post-## -->
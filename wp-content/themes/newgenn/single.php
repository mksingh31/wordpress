<?php
/**
 * The template for displaying all single posts.
 *
 * @package NewGenn
 */

get_header(); ?>

	<div class="breadcrumb-wrap">
		<div class="container">
			<div class="sixteen columns breadcrumb">	
				<header class="entry-header">
					<h2 class="entry-title"><?php the_title(); ?></h2>
				</header><!-- .entry-header -->
				<?php $breadcrumb = get_theme_mod( 'breadcrumb',true ); 
					if( $breadcrumb ) : ?>
					<div id="breadcrumb" role="navigation">
						<?php newgenn_breadcrumbs(); ?>
					</div>
				<?php endif; ?>
			</div>
		</div> 
	</div>  
	
	<div id="content" class="site-content">
		<div class="container">
		<?php 
 		$sidebar_position = get_theme_mod( 'sidebar_position', 'right' ); 
		 if( 'left' == $sidebar_position ) :
			 get_sidebar('left'); 
		 endif; ?>  

	<div id="primary" class="content-area eleven columns">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

				<?php if( get_theme_mod ('social_sharing_box',true)): ?>
								<?php endif; ?>

				<?php if( get_theme_mod ('author_bio_box')): ?>
				<section class="author-bio clearfix">
					<div class="author-info">
						<div class="avatar">
							<?php echo get_avatar( get_the_author_meta( 'email' ), '72' ); ?>
						</div>
						<div class="description">
							<h4><?php echo __( 'About Author:', 'newgenn' ); ?> <?php the_author_posts_link(); ?></h4>
							<?php the_author_meta('description');?>
						</div>
					</div>
				</section>
				<?php endif; ?>

			<?php if( get_theme_mod('related_posts') && function_exists( 'wbls_related_posts' ) ) : ?>
				<section class="related-posts clearfix">
					<?php wbls_related_posts(); ?>
				</section>
			<?php endif;  ?>

			<?php
					if( get_theme_mod ('comments',true) ) :
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() ) :
							comments_template();
						endif;
					endif;
				?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


		<?php if( 'right' == $sidebar_position ) :?>
			<?php get_sidebar(); ?>
		<?php endif; ?>
		
<?php get_footer(); ?>

<?php
/**
 * @package NewGenn
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<div class="entry-meta header-entry-meta">
	<!-- 		<i class="fa fa-calendar"></i>
			<?php webulous_post_date(); ?> -->
		</div><!-- .entry-meta -->
		
	</header><!-- .entry-header -->    

	<?php
$single_featured_image = get_theme_mod( 'single_featured_image' );
$single_featured_image_size = get_theme_mod ('single_featured_image_size','1');
	 if ( $single_featured_image_size == '1' ) :?>
	 		<div class="post-thumb blog-thumb">
	       <?php 
	       if( has_post_thumbnail() && ! post_password_required() ) :   
				the_post_thumbnail('newgenn-blog-large-width'); 
			endif;?>
			</div>
			<?php
		  else: ?> 
		 	 <div class="post-thumb blog-thumb">
		 	 	<?php
		 // 	if( has_post_thumbnail() && ! post_password_required() ) :   
			// 		the_post_thumbnail('newgenn-small-featured-image-width');
			// else :
			// 	echo '<img src="' . get_stylesheet_directory_uri() . '/images/no-image-small-featured-image-width.png" />';
			// endif;?>
			</div><?php
	endif;  ?> 


	<div class="entry-content blog-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'newgenn' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php newgenn_post_nav(); ?>
</article><!-- #post-## -->

<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package NewGenn
 */

get_header(); ?>
	<div id="content" class="site-content">
		<div class="container">
		<header class="entry-header">
							
	<h2 class="entry-title"> 
	<!-- <a href="<?php the_permalink(); ?>" rel="bookmark"><?php #the_title( '', '' ); ?></a></h2> -->
<?php the_title(); ?>
					</h2>
					<hr>
</header>  


	<?php $sidebar_position = get_theme_mod( 'sidebar_position', 'right' ); ?>
		<?php if( 'left' == $sidebar_position ) :?>
			<?php get_sidebar('left'); ?>
		<?php endif; ?>  

	<div id="primary" class="content-area eleven columns">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
				?>

			<?php endwhile; ?>

		<?php 
			if(  get_theme_mod ('numeric_pagination',true) && function_exists( 'webulous_pagination' ) ) : 
					webulous_pagination();
				else :
					newgenn_post_nav();     
				endif; 
		?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<div id="secondary" class="widget-area five columns right-sidebar" role="complementary">
	<div class="left-sidebar">
	<a class="twitter-timeline" href="https://twitter.com/CWBusIncubation" data-widget-id="689028662604115968">Tweets by @CWBusIncubation</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></div>
</div>
		<?php if( 'right' == $sidebar_position ) :?>
			<?php get_sidebar(); ?>
		<?php endif; ?>
		
<?php get_footer(); ?>

<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package NewGenn
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'newgenn' ); ?></a>
	<?php do_action('newgenn_before_header'); ?>
	<header id="masthead" class="site-header header-wrap" role="banner">
		<div class="container">
			<div class="branding eight columns">     
				<div class="site-branding logo">
				<?php 
					$logo_title = get_theme_mod( 'logo_title' );
					$logo = get_theme_mod( 'logo', '' );
						$tagline = get_theme_mod( 'tagline' );
						if( $logo_title && $logo != '' ) : ?>
							<!-- <h1 class="site-title"><a href="/index.php/home-2" rel="home"><img src="<?php echo esc_url($logo) ?>"></a></h1> -->
							<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo esc_url($logo) ?>"></a></h1>
						<?php else : ?>
							<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<!-- <h1 class="site-title"> <a href="/index.php/home-2" rel="home">
								<?php bloginfo( 'name' ); ?></a></h1> -->
						<?php endif; ?>
						<?php if( $tagline ) : ?>
							<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
						<?php endif; ?>
				</div>
			</div>

			<div class="eight columns"> 
				<div class="top-right">
			<!--<?#php dynamic_sidebar( 'top-right' ); ?>-->
			<div class=" customer_service pull-right text-right" style=" margin-top:2px;">
            <a href="https://twitter.com/CWBusIncubation" target="_blank" style="text-decoration:none;color:#4099FF;">
	       <i class="fa fa-twitter-square fa-2x"></i>
	        </a>
            <a href="https://www.linkedin.com/in/sandragarlick"  target="_blank" style="text-decoration:none;color:#007bb6;">
            <i class="fa fa-linkedin-square fa-2x"></i>
			    </a>
            <h4 class="phone" style="margin-top:0px;"><br/>T: +44 (0) 24 7621 4440 <br/>
            <a href="mailto:info@businessincubation.com" target="_top" style="text-decoration:none;"> info@cwbusinessincubation.com</a> </h4>

			</div>      
				</div>					
			</div>
		</div>

	</header><!-- #masthead -->

		<div class="nav-wrap" >
			<div class="container">
					<nav id="site-navigation" class="main-navigation sixteen columns" role="navigation">
						<button class="menu-toggle"><?php _e( 'Primary Menu', 'newgenn' ); ?></button>
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_class' => 'primary-menu' ) ); ?>
			    	</nav><!-- #site-navigation -->
			<!--	 <?php #echo do_shortcode( '[responsive_slider]' ); ?> -->
					<?php echo do_shortcode("[huge_it_slider id='1']"); ?> 
					<?php do_action('newgenn_after_primary_nav'); ?>
			</div>
		</div>



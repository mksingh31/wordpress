<?php
/**
 * Template Name:  Full Width Template
 *
 * @package newgenn
 */

get_header(); ?>

<div class="breadcrumb-wrap">
		<div class="container">
			<div class="sixteen columns breadcrumb">	
				<header class="entry-header">
					<?php echo do_shortcode("[huge_it_slider id='1']"); ?>
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header><!-- .entry-header -->
				<?php $breadcrumb = get_theme_mod( 'breadcrumb',true ); 
					if( $breadcrumb ) : ?>
					<div id="breadcrumb" role="navigation">
						<?php newgenn_breadcrumbs(); ?>
					</div>
				<?php endif; ?>
			</div>
		</div> 
	</div>  

	<?php do_action('newgenn_before_content'); ?>

	<div id="content" class="site-content">
		<div class="container">

		<div id="primary" class="content-area sixteen columns">

			<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					?>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- .row -->

<?php get_footer(); ?>

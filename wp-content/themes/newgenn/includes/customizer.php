<?php
/**
 * NewGenn Theme Customizer
 *
 * @package NewGenn
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function newgenn_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'newgenn_customize_register' );


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function newgenn_customize_preview_js() {
	wp_enqueue_script( 'newgenn_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'newgenn_customize_preview_js' );


if( get_theme_mod('enable_primary_color',false) ) {

	add_action( 'wp_head','wbls_customizer_primary_custom_css' );

	function wbls_customizer_primary_custom_css() {
			$primary_color = get_theme_mod( 'primary_color','#830b7e'); ?>

	<style type="text/css">

button,
input[type="button"],.page-template-blog-fullwidth .entry-body a.more-link,
.page-template-blog-large .entry-body a.more-link,
.archive.category .entry-body a.more-link ,.widget_tag_cloud a:hover,
input[type="reset"],.widget_calendar table caption,
input[type="submit"],.webulous_page_navi li.bpn-next-link a,.home .site-content #primary .services-wrapper div h1, .home .site-content #primary .services-wrapper div h2, .home .site-content #primary .services-wrapper div h3, .home .site-content #primary .services-wrapper div h4, .home .site-content #primary .services-wrapper div h5, .home .site-content #primary .services-wrapper div h6 ,.post-wrapper .latest-post a.btn-readmore:hover,
.webulous_page_navi li.bpn-prev-link a,.hentry.sticky,blockquote,blockquote:before,.flexslider ol.flex-control-paging li a.flex-active,.services
				{
					background-color: <?php  echo esc_html($primary_color); ?>;
				}

				
input[type="text"]:focus,
input[type="email"]:focus,
input[type="url"]:focus,
input[type="password"]:focus,
input[type="search"]:focus,.widget_tag_cloud a,
textarea:focus,.webulous_page_navi li a:hover,ol.comment-list li.byuser article,ol.comment-list li.byuser .comment-author img
				{
					border-color: <?php  echo esc_html($primary_color); ?>;
				}


				
.webulous_page_navi li.bpn-current {
border-bottom-color: <?php  echo esc_html($primary_color); ?>;
}

.widget-area h4.widget-title:after {
border-left-color: <?php  echo esc_html($primary_color); ?>;
}


				
a,.footer-bottom .widget_nav_menu a:hover,.footer-bottom p a,.entry-header .header-entry-meta,.site-footer .footer-widgets a:hover,.widget-area ul li a:hover ,.widget_calendar table th a, .widget_calendar table td a,
.entry-body .header-entry-meta,.entry-header .entry-title-meta span:hover,.widget-area .widget_rss a,
.entry-body .entry-title-meta span:hover,.entry-header .entry-title-meta a:hover,
.entry-body .entry-title-meta a:hover,.breadcrumb-wrap #breadcrumb a,.post-wrapper .latest-post h5 a:hover,.page-links a,.hentry.post h1 a:hover,.branding .site-branding h1.site-title a:hover, .comment-metadata a:hover
				{
					color: <?php echo esc_html($primary_color); ?>;

				}

				
			</style>
<?php
	}
}

if( get_theme_mod('enable_nav_bg_color',false) ) {

	add_action( 'wp_head','wbls_customizer_navbg_custom_css' );

		function wbls_customizer_navbg_custom_css() {
			$nav_bg_color = get_theme_mod( 'nav_bg_color','#030303'); ?>

				<style type="text/css">

					.nav-wrap
							{
								background-color: <?php echo esc_html($nav_bg_color); ?>;
							}
				</style>
<?php }
}




if( get_theme_mod('enable_nav_hover_color',false) ) {

	add_action( 'wp_head','wbls_customizer_hover_custom_css' );

		function wbls_customizer_hover_custom_css() {
			$nav_hover_color = get_theme_mod( 'nav_hover_color','#5f015b'); ?>

				<style type="text/css">

					.main-navigation .current_page_item > a,.main-navigation .current-menu-item > a, .main-navigation .current_page_ancestor > a,.main-navigation ul.nav-menu > li a:hover,.current-menu-parent,.main-navigation ul.nav-menu > li li a:hover,.main-navigation .current_page_parent > a,.main-navigation .current_page_item a, .main-navigation .current-menu-item a, .main-navigation .current-menu-parent > a, .main-navigation .current_page_parent > a
							{
								background-color: <?php echo esc_html($nav_hover_color); ?>;
							}
				</style>
<?php }
}



<?php

	function newgenn_footer_credits() { 
		printf( __('<p>Powered by <a href="%1$s">%2$s</a>', 'newgenn'),
			esc_url( __( 'http://wordpress.org/', 'newgenn' )), 'WordPress');
		printf( '<span class="sep"> .</span>' );
		printf( __( 'Theme: %1$s by %2$s', 'newgenn' ), 'NewGenn', '<a href="http://www.webulousthemes.com/" rel="designer">Webulous Themes</a></p>' );
	}
	
	add_action('newgenn_credits','newgenn_footer_credits');     

?>
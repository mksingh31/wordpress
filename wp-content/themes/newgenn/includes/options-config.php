<?php

$page_builder = __( 'Page Builder', 'newgenn' );
$page_builder_details = __( 'Newgenn Pro supports Page Builder. All our shortcodes can be used as widgets too. You can drag and drop our widgets with page builder visual editor.', 'newgenn' );
$page_layout = __( 'Page Layout', 'newgenn' );
$page_layout_details = __( 'Newgenn Pro offers many different page layouts so you can quickly and easily create your pages with various layout without any hassle!', 'newgenn' );
$unlimited_sidebar = __( 'Unlimited Sidebar', 'newgenn' );
$unlimited_sidebar_details = __( 'Unlimited sidebars allows you to create multiple sidebars. Check out our demo site to see how different pages displays different sidebars!', 'newgenn' );
$shortcode_builder = __( 'Shortcode Builder', 'newgenn' );
$shortcode_builder_details = __( 'With our shortcode builder and lots of shortcodes, you can easily create nested shortcodes and build custom pages!', 'newgenn' );
$portfolio = __( 'Multi Portfolio', 'newgenn' );
$portfolio_details = __( '7 portfolio layouts with Isotope filtering, 3 blog layouts and multiple other alternate layouts for interior pages!', 'newgenn' );
$typography = __( 'Typography', 'newgenn' );
$typography_details = __( 'Newgenn Pro loves typography, you can choose from over 500+ Google Fonts and Standard Fonts to customize your site!', 'newgenn' );
$slider = __( 'Awesome Sliders', 'newgenn' );
$slider_details = __( 'Newgenn Pro includes two types of slider. You can use both Flex and Elastic sliders anywhere in your site.', 'newgenn' );
$woocommerce = __( 'Woo Commerce', 'newgenn' );
$woocommerce_details = __( 'Newgenn Pro has full design/code integration for WooCommerce, your shop will look as good as the rest of your site!', 'newgenn' );
$custom_widget = __( 'Custom Widget', 'newgenn' );
$custom_widget_details = __( 'We offer many custom widgets that are stylized and ready for use. Simply drag &amp; drop into place to activate!', 'newgenn' );
$advanced_admin = __( 'Advanced Admin', 'newgenn' );
$advanced_admin_details = __( 'Advanced Redux Framework for theme options panel, you can customize any part of your site quickly and easily!', 'newgenn' );
$font_awesome = __( 'Font Awesome', 'newgenn' );
$font_awesome_details = __( 'Font Awesome icons are fully integrated into the theme. Use them anywhere in your site in 6 different sizes!', 'newgenn' );
$responsive_layout = __( 'Responsive Layout', 'newgenn' );
$responsive_layout_details = __( 'Newgenn Pro is fully responsive and can adapt to any screen size. Resize your browser window to view it!', 'newgenn' );
$testimonials = __( 'Testimonials', 'newgenn' );
$testimonials_details = __( 'With our testimonial post type, shortcode and widget, Displaying testimonials is a breeze.', 'newgenn' );
$social_media = __( 'Social Media', 'newgenn' );
$social_media_details = __( 'Want your users to stay in touch? No problem, newgenn Pro has Social Media icons all throughout the theme!', 'newgenn' );
$google_map = __( 'Google Map', 'newgenn' );
$google_map_details = __( 'Newgenn Pro includes Goole Map as shortcode and widget. So, you can use it anywhere in your site!', 'newgenn' );
$view_demo = __( 'View Demo', 'newgenn');
$upgrade_to_pro = __( 'Upgrade To Pro', 'newgenn' );  

$newgenn_why_upgrade = <<< FEATURES
<p class="wrap-header">
	<a class="view-demo" href="http://newgenn.webulous.in">
		<i class="fa fa-eye"></i> $view_demo</a> 
	<a class="upgrade" href="http://www.webulousthemes.com/theme/newgenn-pro/">
		<i class="fa fa-upload"></i> $upgrade_to_pro</a>
</p>
<div class="one-third column clear">
	<div class="icon-wrap"><i class="fa  fa-5x fa-cog"></i></div>
	<h3>$page_builder</h3>
	<p>$page_builder_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-th-large"></i></div>
	<h3>$page_layout</h3>
	<p>$page_layout_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-th"></i></div>
	<h3>$unlimited_sidebar</h3>
	<p>$unlimited_sidebar_details</p>
</div>
<div class="one-third column clear">
	<div class="icon-wrap"><i class="fa  fa-5x fa-code-fork"></i></div>
	<h3>$shortcode_builder</h3>
	<p>$shortcode_builder_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-camera"></i></div>
	<h3>$portfolio</h3>
	<p>$portfolio_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-font"></i></div>
	<h3>$typography</h3>
	<p>$typography_details</p>
</div>
<div class="one-third column clear">
	<div class="icon-wrap"><i class="fa  fa-5x fa-slideshare"></i></div>
	<h3>$slider</h3>
	<p>$slider_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-leaf"></i></div>
	<h3>$woocommerce</h3>
	<p>$woocommerce_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-tasks"></i></div>
	<h3>$custom_widget</h3>
	<p>$custom_widget_details</p>
</div>
<div class="one-third column clear">
	<div class="icon-wrap"><i class="fa  fa-5x fa-dashboard"></i></div>
	<h3>$advanced_admin</h3>
	<p>$advanced_admin_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-magic"></i></div>
	<h3>$font_awesome</h3>
	<p>$font_awesome_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-arrows"></i></div>
	<h3>$responsive_layout</h3>
	<p>$responsive_layout_details</p>
</div>
<div class="one-third column clear">
	<div class="icon-wrap"><i class="fa  fa-5x fa-magic"></i></div>
	<h3>$testimonials</h3>
	<p>$testimonials_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-twitter"></i></div>
	<h3>$social_media</h3>
	<p>$social_media_details</p>
</div>
<div class="one-third column">
	<div class="icon-wrap"><i class="fa  fa-5x fa-map-marker"></i></div>
	<h3>$google_map</h3>
	<p>$google_map_details</p>
</div>
FEATURES;

function newgenn_theme_page() {
	add_theme_page( 
		__( 'Upgrade To NewGenn Pro','newgenn'),
		__( 'Theme Upgrade','newgenn'),
		'edit_theme_options',
		'newgenn_upgrade',
		'newgenn_display_upgrade'
	);
}

add_action('admin_menu','newgenn_theme_page');


function newgenn_display_upgrade() {
	global $newgenn_why_upgrade;
	echo '<div class="wrap">';
	echo $newgenn_why_upgrade;
	echo '</div>';
}

	$options = array( 
		'capability' => 10,
		'type' => 'theme_mod',        
		'panels' => apply_filters( 'newgenn_customizer_options', array(
			'newgenn' => array(
				'priority'       => 9,
				'title'          => __('NewGenn Options', 'newgenn'),
				'description'    => __('NewGenn Theme Options', 'newgenn'),
				'sections' => array(
					'general' => array(
						'title' => __('General', 'newgenn'),
						'description' => __('General settings that affects overall site', 'newgenn'),
						'fields' => array(
							'breadcrumb' => array(
								'type' => 'checkbox',
								'label' => __('Enable Breadcrumb', 'newgenn'),
								'default' => 0,
								'sanitize_callback' => 'newgenn_boolean',
							),
							'breadcrumb_char' => array(
								'type' => 'select',
								'label' => __('Select Breadcrumb Character', 'newgenn'),
								'choices' => array(
									'1' => ' &raquo; ',
									'2' => ' / ',
									'3' => ' > '
								),
								'sanitize_callback' => 'newgenn_breadcrumb_char_choices',
								'default' => '1',
							),
							 'numeric_pagination' => array(
                                'type' => 'checkbox',
                                'label' => __('Enable Numeric Page Navigation', 'newgenn'),
                                'description' => __('Check to display numeric page navigation, instead of Previous Posts / Next Posts links.', 'newgenn'),
                                'default' => 1,  
                            ),
                            'sidebar_position' => array(
                                'type' => 'radio',
                                'label' => __('Main Layout', 'newgenn'),
                                'description' => __('Select main content and sidebar alignment.', 'newgenn'),
                                'choices' => array(
                                    'left' => __('Sidebar Left', 'newgenn'),
                                    'right' => __('Sidebar Right', 'newgenn'),
                                ),
                                'default' => 'right',  
                            ),
						),
					),
					'header' => array(
						'title' => __('Header', 'newgenn'),
						'description' => __('Header options', 'newgenn'),
						'fields' => array(
							'logo_title' => array(
								'type' => 'checkbox',
								'label' => __('Logo as Title', 'newgenn'),
								'default' => 0,
								'sanitize_callback' => 'newgenn_boolean',
							),
							'logo' => array(
								'type' => 'image',
								'label' => __('Upload Logo', 'newgenn'),
								'sanitize_callback' => 'esc_url_raw',
							),
							'tagline' => array(
								'type' => 'checkbox',
								'label' => __('Show site Tagline', 'newgenn'),
								'default' => 0,
								'sanitize_callback' => 'newgenn_boolean',
							),
                         
						),
					),
 					'primary_color_field' => array(      
                        'title' => __('Change Color Options', 'newgenn'),
                        'description' => __('This will reflect in links, buttons,Navigation and many others. Choose a color to match your site.', 'newgenn'),
                        'fields' => array(
                            'enable_primary_color' => array(
                                'type' => 'checkbox',
                                'label' => __('Enable Custom Primary color', 'newgenn'),
                                'default' => 0,
                                'sanitize_callback' => 'newgenn_boolean',
                            ),
                            'primary_color' => array(
                                'type' => 'color',
                                'label' => __('Primary Color', 'newgenn'),   
                                'description' => __('', 'newgenn'),
                                'sanitize_callback' => 'sanitize_hex_color',
                                'default' => '#EC1D23'
                            ),
                            'enable_nav_bg_color' => array(
                                'type' => 'checkbox',
                                'label' => __('Enable Custom Navigation Background color', 'newgenn'),
                                'default' => 0,
                                'sanitize_callback' => 'newgenn_boolean',
                            ),
                            'nav_bg_color' => array(   
                                'type' => 'color',
                                'label' => __('Navigation Background Color', 'newgenn'),   
                                'description' => __('', 'newgenn'),
                                'sanitize_callback' => 'sanitize_hex_color',
                                'default' => '#030303'
                            ),
                            'enable_nav_hover_color' => array(
                                'type' => 'checkbox',
                                'label' => __('Enable Custom Navigation Hover color', 'newgenn'),
                                'default' => 0,
                                'sanitize_callback' => 'newgenn_boolean',
                            ),
                            'nav_hover_color' => array(   
                                'type' => 'color',
                                'label' => __('Navigation Hover Color', 'newgenn'),   
                                'description' => __('', 'newgenn'),
                                'sanitize_callback' => 'sanitize_hex_color',
                                'default' => '#ec1d23'
                            ),
                     
                        ),
                    ),
                    
					'footer' => array(
						'title' => __('Footer', 'newgenn'),
						'description' => __('Footer related options', 'newgenn'),
						'fields' => array(
							'footer_widgets' => array(
								'type' => 'checkbox',
								'label' => __('Footer Widget Area', 'newgenn'),
								'default' => 0,
								'sanitize_callback' => 'newgenn_boolean',
							),
							'copyright' => array(
                                'type' => 'textarea',
                                'label' => __('Footer Copyright Text (Validated that it\'s HTML Allowed)', 'newgenn'),
                                'description' => __('HTML Allowed. <b>This field is even HTML validated! </b>', 'newgenn'),
                                'sanitize_callback' => 'wbls_footer_copyright',
                            ),
						),
					),
					'home' => array(  
						'title' => __('Home', 'newgenn'),
						'description' => __('Home Page options', 'newgenn'),
						'fields' => array(
							'slider_cat' => array(
								'type' => 'category',
								'label' => __('Slider Posts Category', 'newgenn'),
								'sanitize_callback' => 'absint',
							),
							'slider_count' => array(
								'type' => 'text',
								'label' => __('No. of Sliders', 'newgenn'),
								'sanitize_callback' => 'absint',
							),
							'service_1' => array(
								'type' => 'dropdown-pages',
								'label' => __('Service Section #1', 'newgenn'),
								'sanitize_callback' => 'absint',
							),
							'service_2' => array(
								'type' => 'dropdown-pages',
								'label' => __('Service Section #2', 'newgenn'),
								'sanitize_callback' => 'absint',
							),
							'service_3' => array(
								'type' => 'dropdown-pages',
								'label' => __('Service Section #3', 'newgenn'),
								'sanitize_callback' => 'absint',
							),
                            'service_4' => array(
                                'type' => 'dropdown-pages',
                                'label' => __('Service Section #4', 'newgenn'),
                                'sanitize_callback' => 'absint',
                            ),                            
							'recent_posts_count' => array(
								'type' => 'text',
								'label' => __('No. of Recent Posts', 'newgenn'),
								'sanitize_callback' => 'absint',
							),
							'page-builder' => array(
                                'type' => 'checkbox',
                                'label' => __('Use Page Builder: Check this to disable theme options for home page content and use page builder to enter content', 'newgenn'),
                                'description' => __(' Check to display breadcrumb navigation.', 'newgenn'),
                                'default' => 0,  
                            ),
                            'flexslider' => array(
                                'type' => 'text',
                                'label' => __('Enter FlexSlider shortcode (FlexSlider for Home Page)', 'newgenn'),
                                'description' => __('FlexSlider for Home Page.  Enter a FlexSlider shortcode to be displayed on Home Page','newgenn'),
                                'sanitize' => 'sanitize_text_field'
                            ),
						),
					),
					'blog' => array(
						'title' => __('Blog', 'newgenn'),
						'description' => __('Blog related options', 'newgenn'),
						'fields' => array(
							'featured_image' => array(
                                'type' => 'checkbox',
                                'label' => __('Enable Featured Image', 'newgenn'),
                                'default' => 1,
                                'sanitize_callback' => 'newgenn_boolean',
                            ),
                            'featured_image_size' => array(
                                'type' => 'radio',
                                'label' => __('Choose the featured image display type for Blog Page ', 'newgenn'),
                                'choices' => array(
                                    '1' => 'Large Featured Image',
                                    '2' => 'Small Featured Image',        
                                ),
                                'default' => '1',      
                            ),
                            'single_featured_image' => array(
                                'type' => 'checkbox',
                                'label' => __('Enable Single Post Featured Image', 'newgenn'),
                                'default' => 1,
                                'sanitize_callback' => 'newgenn_boolean',
                            ),
                            'single_featured_image_size' => array(
                                'type' => 'radio',
                                'label' => __('Choose the featured image display type for Single Page ', 'newgenn'),
                                'choices' => array(
                                    '1' => 'Large Featured Image',
                                    '2' => 'Small Featured Image',      
                                ),
                                'default' => '1',   
                            ),
                             'author_bio_box' => array(
                                'type' => 'checkbox',
                                'label' => __(' Enable Author Bio Box below single post', 'newgenn'),
                                'description' => __('Show Author information box below single post.', 'newgenn'),
                                'default' => 0,  
                            ),
                            'social_sharing_box' => array(
                                'type' => 'checkbox',
                                'label' => __('Show social sharing options box below single post', 'newgenn'),
                                'description' => __('Show social sharing options box below single post.', 'newgenn'),
                                'default' => 1,  
                            ),
                            'related_posts' => array(
                                'type' => 'checkbox',
                                'label' => __('Show related posts', 'newgenn'),
                                'description' => __('Show related posts.', 'newgenn'),
                                'default' => 0,  
                            ),
                            'comments' => array(
                                'type' => 'checkbox',
                                'label' => __(' Show Comments', 'newgenn'),
                                'description' => __('Show Comments', 'newgenn'),
                                'default' => 1,  
                            ),
						),
					),
                    'social_sharing_box' => array(
                        'title' => __('Social Sharing Box', 'newgenn'),
                        'description' => __('Social Sharing Icons Setup', 'newgenn'),
                        'fields' => array(
                            'facebook_sb' => array(
                                'type' => 'checkbox',
                                'label' => __('Show facebook sharing option in single posts', 'newgenn'),
                                'description' => __('Show facebook sharing option in single posts.', 'newgenn'),
                                'default' => 1,  
                            ),
                            'twitter_sb' => array(
                                'type' => 'checkbox',
                                'label' => __('Show twitter sharing option in single posts', 'newgenn'),
                                'description' => __('Show twitter sharing option in single posts.', 'newgenn'),                                'default' => 1,  
                            ),
                            'linkedin_sb' => array(
                                'type' => 'checkbox',
                                'label' => __('Show linkedin sharing option in single posts', 'newgenn'),
                                'description' => __('Show linkedin sharing option in single posts.', 'newgenn'),
                                'default' => 1,  
                            ),
                            'google-plus_sb' => array(
                                'type' => 'checkbox',
                                'label' => __('Show googleplus sharing option in single posts', 'newgenn'),
                                'description' => __('Show googleplus sharing option in single posts.', 'newgenn'),
                                'default' => 1,  
                            ),
                            'email_sb' => array(
                                'type' => 'checkbox',
                                'label' => __('Show email sharing option in single posts', 'newgenn'),
                                'description' => __('Show email sharing option in single posts.', 'newgenn'),
                                'default' => 1,  
                            ),
                        ),
                    ),

				)
			),
		) 
	)
	);

function newgenn_boolean($value) {
	if(is_bool($value)) {
		return $value;
	} else {
		return false;
	}
}

function newgenn_breadcrumb_char_choices($value='') {
	$choices = array('1','2','3');

	if( in_array($value, $choices)) {
		return $value;
	} else {
		return '1';
	}
}

if ( ! function_exists( 'wbls_footer_copyright' ) ) {

    function wbls_footer_copyright($string) {
        $allowed_tags = array(    
                                'a' => array(
                                    'href' => array(),
                                    'title' => array()
                            ),
                                'em' => array(),
                                'strong' => array(),
        );
        return wp_kses( $string,$allowed_tags);

    }
}